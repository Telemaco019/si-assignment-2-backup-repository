import javax.ejb.Remote;

@Remote
public interface UsersManagementEJBRemote {
    boolean userLogin(String email, String password);

    boolean managerLogin(String email, String password);

    void addUser(String name, String surname, String password, String creditCardNumber, String email);

    boolean userPresent(String email);

    /**
     * Indicates if the user associated to the email provided as parameter is a manager user which can edit the
     * multimedia content of the system
     * @param email
     * @return
     */
    boolean isManager(String email);

    String getUserInfo(String email);

    void updateUserEmail(String oldEmail, String newEmail);

    void updateUserName(String email, String newName);

    void updateUserSurname(String email, String newSurname);

    void updateUserPassword(String email, String newPassword);

    void updateUserCreditCardNumber(String email, String newCreditCardNumber);

    void deleteUser(String email);
}
