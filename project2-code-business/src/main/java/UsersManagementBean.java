import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name = "UserManagementBean")
public class UsersManagementBean implements UsersManagementEJBRemote {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @PersistenceContext
    private EntityManager em;

    public UsersManagementBean() {

    }

    @Override
    public boolean userLogin(String email, String password) {
        boolean userAuthenticated = false;

        String passwordSHA256hex = DigestUtils.sha256Hex(password);
        try {
            User user = em.find(NormalUser.class, email);
            if (user.getPassword().equals(passwordSHA256hex))
                userAuthenticated = true;
        } catch (Exception e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        }

        return userAuthenticated;
    }

    @Override
    public boolean managerLogin(String email, String password) {
        boolean userAuthenticated = false;

        String passwordSHA256hex = DigestUtils.sha256Hex(password);
        try {
            Manager manager = em.find(Manager.class, email);
            if (manager.getPassword().equals(passwordSHA256hex))
                userAuthenticated = true;
        } catch (Exception e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        }

        return userAuthenticated;
    }

    @Override
    public void addUser(String name, String surname, String password, String creditCardNumber, String email) {
        String passwordSHA256hex = DigestUtils.sha256Hex(password);
        try {
            User newUser = new NormalUser(name, surname, passwordSHA256hex, creditCardNumber, email);
            em.persist(newUser);
        }catch(Exception e) {
            logger.log(Level.ERROR, e.getMessage());
        }
    }

    @Override
    public boolean userPresent(String email) {
        return em.find(NormalUser.class, email) != null;
    }

    @Override
    public boolean isManager(String email) {
        return em.find(Manager.class, email) != null;
    }

    @Override
    public String getUserInfo(String email) {
        return getUser(email).toString();
    }

    @Override
    public void updateUserEmail(String oldEmail, String newEmail) {
        User u = getUser(oldEmail);
        u.setEmail(newEmail);
    }

    @Override
    public void updateUserName(String email, String newName) {
        User u = getUser(email);
        u.setName(newName);
    }

    @Override
    public void updateUserSurname(String email, String newSurname) {
        User u = getUser(email);
        u.setSurname(newSurname);
    }

    @Override
    public void updateUserPassword(String email, String newPassword) {
        User u = getUser(email);
        String passwordSHA256hex = DigestUtils.sha256Hex(newPassword);
        u.setPassword(passwordSHA256hex);
    }

    @Override
    public void updateUserCreditCardNumber(String email, String newCreditCardNumber) {
        NormalUser u = getUser(email);
        u.setCreditCardNumber(newCreditCardNumber);
    }

    @Override
    public void deleteUser(String email) {
        NormalUser userToRemove = getUser(email);
        // Delete user payments
        //   Query deleteUserPayments = em.createQuery("DELETE FROM Payment p WHERE p.payer.email=:e");
        //   deleteUserPayments.setParameter("e",email);
        //   deleteUserPayments.executeUpdate();

        // Delete user watchlist
        Watchlist watchlistToRemove =  userToRemove.getWatchlist();
        em.remove(userToRemove);
        em.remove(watchlistToRemove);
    }

    /**
     * Return the user associated to the email provided as parameter. Can return null.
     *
     * @param email
     * @return
     */
    private NormalUser getUser(String email) {
        assert userPresent(email);

        return em.find(NormalUser.class, email);
    }
}
