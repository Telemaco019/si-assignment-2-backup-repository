import javax.ejb.Remote;
import java.util.List;

@Remote
public interface ContentManagementEJBRemote {
    void addContent(String director, int year, String category, String title, String type);

    List<String> getContents();

    boolean contentPresent(int id);

    void updateContentTitle(int contentId, String newTitle);

    void updateContentDirector(int contentId, String newDirector);

    void updateContentYear(int contentId, int newYear);

    void updateContentCategory(int contentId, String newCategory);

    void updateContentType(int contentId, String newType);

    List<String> getSuggestedCategories(String userEmail);

    List<String> getSuggestedContents(String userEmail,String category, int contentsPerCategory);

    List<String> getWatchlist(String userEmail);

    void addToWatchlist(String userEmail, int contentId);

    List<String> getSearchResults(String title, String director, String category, String yearFrom, String yearTo, String sortBy, String order);

    void removeFromWatchlist(String userEmail, int contentId);

    void deleteContent(int contentId);

    boolean contentPresentInWatchlist(String userEmail, int contentId);
}
