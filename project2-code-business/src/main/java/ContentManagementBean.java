import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Stateless(name = "ContentManagementBean")
public class ContentManagementBean implements ContentManagementEJBRemote {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @PersistenceContext
    private EntityManager em;

    public ContentManagementBean() {

    }

    @Override
    public void addContent(String director, int year, String category, String title, String type) {
        Content content = new Content(director, year, category, title, type);
        em.persist(content);
    }

    /**
     * Returns a list of string containing all the contents available. Each content is shown in string format, with its
     * fields separated by the character '|'.
     *
     * @return
     */
    @Override
    public List<String> getContents() {
        Query q = em.createQuery("select c from Content c");
        return getResultListToString(q.getResultList());
    }

    @Override
    public boolean contentPresent(int id) {
        return getContent(id) != null;
    }

    @Override
    public void updateContentTitle(int contentId, String newTitle) {
        Content content = getContent(contentId);
        content.setTitle(newTitle);
    }

    @Override
    public void updateContentDirector(int contentId, String newDirector) {
        Content content = getContent(contentId);
        content.setDirector(newDirector);
    }

    @Override
    public void updateContentYear(int contentId, int newYear) {
        Content content = getContent(contentId);
        content.setYear(newYear);
    }

    @Override
    public void updateContentCategory(int contentId, String newCategory) {
        Content content = getContent(contentId);
        content.setCategory(newCategory);
    }

    @Override
    public void updateContentType(int contentId, String newType) {
        Content content = getContent(contentId);
        content.setType(newType);
    }

    @Override
    /**
     * Return the different content categories stored in the db for which there is at least one suggested content
     */
    public List<String> getSuggestedCategories(String userEmail) {
        Query q = em.createQuery("SELECT DISTINCT(c.category) FROM Content c");
        List<String> categories = q.getResultList();

        return categories.stream()
                .filter(c -> getSuggestedContents(userEmail, c, 1).size() > 0)
                .collect(Collectors.toList());
    }

    @Override
    /**
     * Return suggested content for a specific user, selecting content from the db which is not present in its watchlist
     */
    public List<String> getSuggestedContents(String userEmail, String category, int numMaxContentsPerCategory) {
        // TODO: maybe optimize?
        Query q = em.createQuery("SELECT c FROM Content c where c.category=:cat");
        NormalUser user = getUser(userEmail);
        Watchlist watchlist = user.getWatchlist();

        q.setParameter("cat", category);

        List<Object> contents = q.getResultList();
        List<Content> watchlistContent = watchlist.getContents();
        contents.removeAll(watchlistContent);

        return getResultListToString(contents);
    }

    /**
     * Return the content identified by the values provided as parameter.
     *
     * @param title
     * @param director
     * @param category
     * @param yearFrom
     * @param yearTo
     * @param sortBy   Field to use for ordering. C
     * @param order
     * @return
     */
    @Override
    public List<String> getSearchResults(String title, String director, String category, String yearFrom, String yearTo, String sortBy, String order) {
        int yearF = yearFrom.isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(yearFrom);
        int yearT = yearTo.isEmpty() ? Integer.MAX_VALUE : Integer.parseInt(yearTo);

        String queryBase = "select c from Content c where title like:t and director like:d and category like:c and year between:yf and :yt order by :sBy";
        String queryString = (order.matches("asc|desc") && sortBy.matches("title|director|category|year|type")) ?
                queryBase.replace(":sBy", sortBy + ' ' + order) :
                queryBase.replace(" order by :sBy", "");

        Query q = em.createQuery(queryString);
        q.setParameter("t", '%' + title + '%');
        q.setParameter("d", '%' + director + '%');
        q.setParameter("c", '%' + category + '%');
        q.setParameter("yf", yearF);
        q.setParameter("yt", yearT);

        return getResultListToString(q.getResultList());
    }

    @Override
    public List<String> getWatchlist(String userEmail) {
        NormalUser user = getUser(userEmail);
        Watchlist userWatchlist = user.getWatchlist();
        return getResultListToString(userWatchlist.getContents());
    }

    @Override
    public void addToWatchlist(String userEmail, int contentId) {
        NormalUser user = getUser(userEmail);
        Content toAdd = getContent(contentId);
        user.getWatchlist().addContent(toAdd);
    }

    @Override
    public void removeFromWatchlist(String userEmail, int contentId) {
        NormalUser user = getUser(userEmail);
        Watchlist userWatchlist = user.getWatchlist();
        userWatchlist.removeContent(contentId);
    }

    @Override
    public void deleteContent(int contentId) {
        Content contentToRemove = getContent(contentId);

        Query getWatchlistsWithContent = em.createQuery("select w from Watchlist w");
        List<Watchlist> watchlists = getWatchlistsWithContent.getResultList();
        for (Watchlist watchlist : watchlists) {
            if (watchlist.contains(contentId))
                watchlist.removeContent(contentId);
        }

        em.remove(contentToRemove);
    }

    @Override
    public boolean contentPresentInWatchlist(String userEmail, int contentId) {
        NormalUser user = getUser(userEmail);
        Watchlist userWatchlist = user.getWatchlist();

        return userWatchlist.contains(contentId);
    }

    private Content getContent(int contentId) {
        return em.find(Content.class,contentId);
    }

    private NormalUser getUser(String userEmail) {
        return em.find(NormalUser.class,userEmail);
    }

    /**
     * Provided as parameter the result list returned from a query (list of Content objects), return a string version
     * of the list obtained by applaying the method toString to each Content object
     *
     * @param resultList
     * @return
     */
    private List<String> getResultListToString(List resultList) {
        List<String> result = new ArrayList<>();
        for (Object o : resultList)
            result.add(o.toString());

        return result;
    }
}
