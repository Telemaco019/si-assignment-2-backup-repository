import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class NormalUser extends User {
    private String creditCardNumber;
    @OneToMany(cascade = {CascadeType.ALL})
    private List<Payment> payments;
    @OneToOne(cascade = {CascadeType.ALL})
    private Watchlist watchlist;
    @Temporal(TemporalType.DATE)
    private Date registrationDate;

    public NormalUser(String name, String surname, String passwordSHA256hex, String creditCardNumber, String email) {
        super(name, surname, passwordSHA256hex, email);
        this.creditCardNumber = creditCardNumber;
        this.registrationDate = new Date();
        watchlist = new Watchlist();
        payments = new ArrayList<>();
    }

    public NormalUser() {
        super();
        watchlist = new Watchlist();
        payments = new ArrayList<>();
    }
    
    public String toString() {
        return String.format("%s|%s|%s|%s|%s",getName(),getSurname(),getEmail(),getCreditCardNumber(),getRegistrationDate().toString());
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Watchlist getWatchlist() {
        return watchlist;
    }

    public void setWatchlist(Watchlist watchList) {
        this.watchlist = watchList;
    }
}
