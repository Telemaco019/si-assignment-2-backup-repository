import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Watchlist {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToMany(cascade = {CascadeType.ALL})
    private List<Content> contents;

    public Watchlist() {
        contents = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void addContent(Content toAdd) {
        contents.add(toAdd);
    }

    public List<Content> getContents() {
        return contents;
    }

    public void removeContent(int contentId) {
        ArrayList<Content> toRemove = new ArrayList<>();
        for (Content c : contents) {
            if (c.getId() == contentId)
                toRemove.add(c);
        }
        contents.removeAll(toRemove);
    }

    public boolean contains(int contentId) {
        for(Content c: contents) {
            if(c.getId()==contentId)
                return  true;
        }
        return false;
    }

    /**
     * Return a list containing all the different categories of the content present in the watchlist
     * @return
     */
    public List<String> getCategories() {
        return contents.stream().map(Content::getCategory).distinct().collect(Collectors.toList());
    }
}
