import javax.persistence.Entity;

@Entity
public class Manager extends User{
    private static final long serialVersionUID = 1L;

    public Manager(String name, String surname, String passwordSHA256Hex, String email) {
        super(name,surname,passwordSHA256Hex,email);
    }

    public Manager() {
        super();
    }
}
