import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ServletWatchlist")
public class ServletWatchlist extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);


    @EJB
    ContentManagementEJBRemote contentManager;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doGet method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        String userEmail = request.getSession().getAttribute("username").toString();
        List<String> watchlist = contentManager.getWatchlist(userEmail);
        response.setContentType("text/html");
        StringBuffer htmlToPrint = new StringBuffer();

        if (watchlist.size() == 0) {
            htmlToPrint.append("Your watchlist is currently empty. Use the search page for adding content to your watchlist.");
        } else {
            htmlToPrint.append("<table id=\"contentTable\">");
            htmlToPrint.append("<tr>");
            htmlToPrint.append("<th> ID </th> <th> Director </th> <th> Year </th> <th> Category </th> <th> Title </th> <th> Type </th><th></th>");
            htmlToPrint.append("</tr>");
            for (String content : watchlist) {
                List<String> fields = Arrays.asList(content.split("\\|"));
                htmlToPrint.append("<tr>");
                for (String field : fields) {
                    htmlToPrint.append("<td>" + field + "</td>");
                }
                htmlToPrint.append("<td> <a href=\"watchlistRemove?toRemove=" + fields.get(0) + "\"> Remove </a> </td>");
                htmlToPrint.append("</tr>");
            }
            htmlToPrint.append("</table>");
        }

        request.getSession().setAttribute("watchlist", htmlToPrint.toString());
        request.getRequestDispatcher("/user-watchlist-page.jsp").include(request, response);
    }
}
