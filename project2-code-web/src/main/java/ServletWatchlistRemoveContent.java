import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletWatchlistRemoveContent")
public class ServletWatchlistRemoveContent extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @EJB
    ContentManagementEJBRemote contentManager;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doGet method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        response.setContentType("text/html");
        String userEmail = request.getSession().getAttribute("username").toString();

        if (request.getParameter("toRemove") != null) {
            int contentId = Integer.parseInt(request.getParameter("toRemove"));
            if (contentManager.contentPresentInWatchlist(userEmail,contentId)) {
                PrintWriter out = response.getWriter();
                contentManager.removeFromWatchlist(userEmail, contentId);
                out.print("<script type=\"text/javascript\">");
                out.print("alert('Content sucessfully removed from your watchlist');");
                out.print("</script>");
            }
            request.getRequestDispatcher("/showWatchlist").include(request, response);
        }
    }
}
