import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import utils.Utilities;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet(name = "ServletRegistration")
public class ServletRegistration extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    private static final long serialVersionUID = 1L;


    @EJB
    private UsersManagementEJBRemote usersManagement;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String creditCard = request.getParameter("creditCard");
        String password = request.getParameter("password");

        String[] fields = new String[]{name, surname, email, creditCard, password};
        PrintWriter out = response.getWriter();
        if (Arrays.asList(fields).contains("")) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Error during the registration: please fill all the fields.');");
            out.println("</script>");
            logger.log(Level.DEBUG,"User didn't fill all the fields");
        } else if (usersManagement.userPresent(email)) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Error during the registration: the inserted email has already been used.');");
            out.println("</script>");
            logger.log(Level.DEBUG,"User tried to register with an already used email");
        } else if (!Utilities.isEmail(email)) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Error during the registration: the inserted email is not valid.');");
            out.println("</script>");
            logger.log(Level.DEBUG,"User tried to register with an invalid email");
        } else if (!Utilities.isCreditCard(creditCard)) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Error during the registration: the inserted credit card number is not valid.');");
            out.println("</script>");
            logger.log(Level.DEBUG,"User tried to register with an invalid credit card");
        } else if (password.length() < Utilities.getMinPasswordLength()) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Error during the registration: the password must be at least " + Utilities.getMinPasswordLength() + " characters long.');");
            out.println("</script>");
        } else {
            usersManagement.addUser(name, surname, password, creditCard, email);
            out.println("<script type=\"text/javascript\">");
            out.println("alert('User registrated successfully!');");
            out.println("</script>");
            logger.log(Level.INFO,"New user added to the DB");
        }

        RequestDispatcher rs = getServletContext().getRequestDispatcher("/index.html");
        rs.include(request, response);
    }
}
