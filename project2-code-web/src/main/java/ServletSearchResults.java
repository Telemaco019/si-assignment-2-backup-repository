import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ServletSearchResults")
public class ServletSearchResults extends HttpServlet {

    @EJB
    ContentManagementEJBRemote contentManager;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null) {
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        response.setContentType("text/html");
        StringBuffer toPrint = new StringBuffer();
        toPrint.append("<div>");

        if(request.getParameterMap().containsKey("title")) {
            String title = request.getParameter("title");
            String director = request.getParameter("director");
            String category = request.getParameter("category");
            String yearFrom = request.getParameter("yearFrom");
            String yearTo = request.getParameter("yearTo");
            String sortBy = request.getParameter("sortBy");
            String order = request.getParameter("order");
            List<String> searchResults = contentManager.getSearchResults(title, director, category, yearFrom, yearTo, sortBy, order);

            toPrint.append("<h1> Search results </h1>");
            if (searchResults.size() == 0) {
                toPrint.append("Your search produced no results.");
            } else {
                toPrint.append("<table id=\"contentTable\">");
                toPrint.append("<tr>");
                toPrint.append("<th> ID </th> <th> Director </th> <th> Year </th> <th> Category </th> <th> Title </th> <th> Type </th>");
                toPrint.append("</tr>");
                for (String content : searchResults) {
                    List<String> fields = Arrays.asList(content.split("\\|"));
                    toPrint.append("<tr>");
                    for (String field : fields) {
                        toPrint.append("<td>" + field + "</td>");
                    }
                    toPrint.append("</tr>");
                }
                toPrint.append("</table>");
            }
        }

        toPrint.append("<div style=\"margin-top: 40px;\">");
        toPrint.append("<h1> Search content </h1>");
        toPrint.append("<br> (All the fields are optional)");
        toPrint.append("<form action=\"searchResults\" method=\"get\">");
        toPrint.append("<ul style=\"list-style:none;\">");
        toPrint.append("<li>Search title:  <input type=\"text\" placeholder=\"title\" name=\"title\"/> </li>");
        toPrint.append("<li>Search director:  <input type=\"text\" placeholder=\"director\" name=\"director\"/> </li>");
        toPrint.append("<li>Search category:  <input type=\"text\" placeholder=\"category\" name=\"category\"/> </li>");
        toPrint.append("<li>Year (from):  <input type=\"number\" placeholder=\"from year\" name=\"yearFrom\"/> </li>");
        toPrint.append("<li>Year (to): <input type=\"number\" placeholder=\"to year\" name=\"yearTo\"/> </li>");
        toPrint.append("<li>Sort by: <select name=\"sortBy\">");
        toPrint.append("<option value=\"title\">Title</option>");
        toPrint.append("<option value=\"director\">Director</option>");
        toPrint.append("<option value=\"category\">Category</option>");
        toPrint.append("<option value=\"type\">Type</option>");
        toPrint.append("<option value=\"year\">Year</option></select>");
        toPrint.append("<input type=\"radio\" name=\"order\" value=\"asc\" checked> Ascending");
        toPrint.append("<input type=\"radio\" name=\"order\" value=\"desc\"> Descending</li>");

        toPrint.append("</ul>");
        toPrint.append("<input type=\"submit\" value=\"Confirm\"/>");
        toPrint.append("</form>");
        toPrint.append("</div>");

        // Close div main
        toPrint.append("</div>");

        request.getSession().setAttribute("results", toPrint.toString());
        RequestDispatcher rs = getServletContext().getRequestDispatcher("/user-search-page.jsp");
        rs.include(request, response);
    }
}
