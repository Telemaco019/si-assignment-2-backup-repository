import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "ServletLogin")
public class ServletLogin extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    private static final long serialVersionUID = 1L;

    @EJB
    private UsersManagementEJBRemote usersManagement;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get request parameters for email and password
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (usersManagement.userLogin(email, password) || usersManagement.managerLogin(email,password)) {
            logger.log(Level.INFO,"User " + email + " logged in");
            // Login successful: create new Http session
            HttpSession session = request.getSession();
            session.setAttribute("username", email);

            // Check if user is administrator
            if (usersManagement.isManager(email)) {
                session.setAttribute("isManager", true);
                response.sendRedirect(request.getContextPath() + "/editContent");
            } else
                response.sendRedirect(request.getContextPath() + "/suggestedContent");
        } else {
            // Login failed
            logger.log(Level.INFO,"Failed login attempt");
            PrintWriter out = response.getWriter();
            out.println("<font color=red><center>Either user name or password is wrong.</center></font>");
            RequestDispatcher rs = getServletContext().getRequestDispatcher("/index.html");
            rs.include(request, response);
        }
    }

}