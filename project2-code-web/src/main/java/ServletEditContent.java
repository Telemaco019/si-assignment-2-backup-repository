import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ServletEditContent")
public class ServletEditContent extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @EJB
    ContentManagementEJBRemote contentManager;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.DEBUG, "Method doPost invoked");

        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null || request.getSession().getAttribute("isManager") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doPost method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String id = request.getParameter("id");
        String title = request.getParameter("title");
        String director = request.getParameter("director");
        String yearString = request.getParameter("year");
        String category = request.getParameter("category");
        String type = request.getParameter("type");

        if (!id.matches("\\d+")) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('The ID must be a number.');");
            out.println("</script>");
            logger.log(Level.DEBUG, "Integer number expected, the user filled something different");
        } else if (!contentManager.contentPresent(Integer.parseInt(id))) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('The inserted ID is not valid.');");
            out.println("</script>");
            logger.log(Level.DEBUG, "User typed wrong content ID");
        } else if (!yearString.matches("\\d+") && !yearString.equals("")) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('The inserted year is not valid.');");
            out.println("</script>");
            logger.log(Level.DEBUG, "Integer number expected, the user filled something different");
        } else {
            if (!title.equals(""))
                contentManager.updateContentTitle(Integer.parseInt(id), title);
            if (!director.equals(""))
                contentManager.updateContentDirector(Integer.parseInt(id), title);
            if (!yearString.equals(""))
                contentManager.updateContentYear(Integer.parseInt(id), Integer.parseInt(yearString));
            if (!category.equals(""))
                contentManager.updateContentCategory(Integer.parseInt(id), category);
            if (!type.equals(""))
                contentManager.updateContentType(Integer.parseInt(id), type);

            out.println("<script type=\"text/javascript\">");
            out.println("alert('Content updated successfully.');");
            // Redirect for reloading updated data (method doGet will be called)
            out.println("location='editContent';");
            out.println("</script>");

            logger.log(Level.INFO, "New content added to the DB");
        }

        request.getRequestDispatcher("/manager-edit-content.jsp").include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.DEBUG, "Method doGet invoked");
        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null || request.getSession().getAttribute("isManager") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doGet method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        StringBuffer htmlToPrint = new StringBuffer();
        response.setContentType("text/html");

        List<String> contents = contentManager.getContents();
        if (contents.size() == 0) {
            htmlToPrint.append("<script type=\"text/javascript\">");
            htmlToPrint.append("alert('No content is present in the database.');");
            htmlToPrint.append("</script>");
        } else {
            htmlToPrint.append("<div class=\"main\">");
            htmlToPrint.append("<h1>Content available</h1>");
            htmlToPrint.append("<table id=\"contentTable\">");
            htmlToPrint.append("<tr>");
            htmlToPrint.append("<th> ID </th> <th> Director </th> <th> Year </th> <th> Category </th> <th> Title </th> <th> Type </th> <th></th>");
            htmlToPrint.append("</tr>");
            for (String content : contents) {
                List<String> fields = Arrays.asList(content.split("\\|"));
                htmlToPrint.append("<tr>");
                for (String field : fields) {
                    htmlToPrint.append("<td>" + field + "</td>");
                }
                htmlToPrint.append("<td><a href=\"deleteContent?toDelete=" + fields.get(0) + "\"> Delete </a></td>");
                htmlToPrint.append("</tr>");
            }
            htmlToPrint.append("</table>");
            htmlToPrint.append("</div>");
        }

        request.getSession().setAttribute("contentListTable", htmlToPrint.toString());
        request.getRequestDispatcher("/manager-edit-content.jsp").forward(request, response);
    }
}
