import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletDeleteUser")
public class ServletDeleteUser extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @EJB
    private UsersManagementEJBRemote usersManagement;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.DEBUG, "Method doPost invoked");

        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doPost method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        // Get user email
        String userToDelete = request.getSession().getAttribute("username").toString();
        // Invalidate http session (logout)
        request.getSession().invalidate();
        // Delete user from db
        usersManagement.deleteUser(userToDelete);
        // Communicate success of operation to user
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<script type=\"text/javascript\">");
        out.println("alert('Your account has been removed from the system.');");
        out.println("</script>");
        logger.log(Level.INFO, "User " + userToDelete + " removed from the DB");
        // Redirect to main page
        request.getRequestDispatcher("/index.html").include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.DEBUG, "Method doGet invoked");
        doPost(request, response);
    }
}
