import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet(name = "ServletAddContent")
public class ServletAddContent extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @EJB
    private ContentManagementEJBRemote contentManager;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.DEBUG, "Method doPost invoked");

        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null || request.getSession().getAttribute("isManager") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doPost method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String title = request.getParameter("title");
        String director = request.getParameter("director");
        String yearString = request.getParameter("year");
        String category = request.getParameter("category");
        String type = request.getParameter("type");

        String[] fields = new String[]{title, director, yearString, category, type};
        if (Arrays.asList(fields).contains("")) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Please fill all the fields.');");
            out.println("</script>");
            logger.log(Level.DEBUG, "Not all fields have been filled");
        } else if (!yearString.matches("\\d+")) { // check if year string is not an integer
            out.println("<script type=\"text/javascript\">");
            out.println("alert('The inserted year is not valid.');");
            out.println("</script>");
            logger.log(Level.DEBUG, "Integer number expected, the user filled something different");
        } else {
            int year = Integer.parseInt(yearString);
            contentManager.addContent(director, year, category, title, type);
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Content added successfully!');");
            out.println("</script>");
            logger.log(Level.DEBUG, "Content added successfully to the DB");
        }

        request.getRequestDispatcher("/manager-add-content.jsp").include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.DEBUG, "Method doGet invoked");
        doPost(request, response);
    }
}
