import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServletDeleteContent extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    @EJB
    ContentManagementEJBRemote contentManager;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if user is authenticated and if it is a manager
        if (request.getSession().getAttribute("username") == null || request.getSession().getAttribute("isManager") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doPost method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        String idContentToRemoveString = request.getParameter("toDelete");
        if(idContentToRemoveString.matches("\\d+")) {
            int idContentToRemove = Integer.parseInt(idContentToRemoveString);
            contentManager.deleteContent(idContentToRemove);
            logger.log(Level.INFO,"Content removed from the db");

            PrintWriter out = response.getWriter();
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Content removed from the DB');");
            out.println("</script>");
        }

        request.getRequestDispatcher("/editContent").include(request,response);
    }
}
