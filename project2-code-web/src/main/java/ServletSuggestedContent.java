import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@WebServlet(name = "ServletSuggestedContent")
public class ServletSuggestedContent extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    private ContentManagementEJBRemote contentManager;
    private static final int MAX_NUMBER_OF_SUGGESTED_CONTENTS_PER_CATEGORY = 5;
    private static Logger logger = Logger.getLogger(ServletSuggestedContent.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doGet method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }
        response.setContentType("text/html");
        String userEmail = request.getSession().getAttribute("username").toString();
        StringBuffer htmlToPrint = new StringBuffer();

        List<String> categories = contentManager.getSuggestedCategories(userEmail);
        if (categories.size() > 0) {
            // Print a table for each category
            for (String category : categories) {
                htmlToPrint.append("<table id=\"contentTable\" style=\"margin-bottom:30px;\">");
                htmlToPrint.append("<caption> Category " + category + "</caption>");
                htmlToPrint.append("<tr>");
                htmlToPrint.append("<th> ID </th> <th> Director </th> <th> Year </th> <th> Category </th> <th> Title </th> <th> Type </th> <th></th>");
                htmlToPrint.append("</tr>");
                List<String> contents = contentManager.getSuggestedContents(userEmail,category, MAX_NUMBER_OF_SUGGESTED_CONTENTS_PER_CATEGORY);
                for (String content : contents) {
                    List<String> fields = Arrays.asList(content.split("\\|"));
                    htmlToPrint.append("<tr>");
                    for (String field : fields) {
                        htmlToPrint.append("<td>" + field + "</td>");
                    }
                    htmlToPrint.append("<td> <a href=\"watchlistAdd?toAdd="+fields.get(0)+ "\"> Add to your watchlist </a></td>");
                    htmlToPrint.append("</tr>");
                }
                htmlToPrint.append("</table>");
            }
        } else {
           htmlToPrint.append("No suggested content available");
        }

        request.getSession().setAttribute("suggestedContent", htmlToPrint.toString());
        request.getRequestDispatcher("/user-main-page.jsp").forward(request, response);
    }
}
