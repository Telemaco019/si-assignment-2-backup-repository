import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import utils.Utilities;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ServletUserInfo")
public class ServletUserInfo extends HttpServlet {
    private static final Logger logger = Logger.getLogger(ContentManagementBean.class);

    private static final long serialVersionUID = 1L;

    @EJB
    private UsersManagementEJBRemote usersManagement;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if user is authenticated
        if (request.getSession().getAttribute("username") == null) {
            logger.log(Level.INFO, "Unauthenticated user tried to execute doPost method");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        RequestDispatcher rs = request.getRequestDispatcher("/user-personal-info.jsp");
        response.setContentType("text/html");
        String currentUserEmail = request.getSession().getAttribute("username").toString();

        String newName = request.getParameter("name");
        String newSurname = request.getParameter("surname");
        String newEmail = request.getParameter("email");
        String newCreditCard = request.getParameter("creditCard");
        String newPassword = request.getParameter("password");
        boolean isUpdated = false;

        PrintWriter out = response.getWriter();

        if (!newName.equals("")) {
            usersManagement.updateUserName(currentUserEmail, newName);
            logger.log(Level.DEBUG, "User updated their name");
            isUpdated = true;
        }
        if (!newSurname.equals("")) {
            usersManagement.updateUserSurname(currentUserEmail, newSurname);
            logger.log(Level.DEBUG, "User updated their surname");
            isUpdated = true;
        }
        if (!newCreditCard.equals("")){
            if (!Utilities.isCreditCard(newCreditCard)){
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Error during the update: the inserted credit card number is not valid.');");
                out.println("</script>");
                logger.log(Level.DEBUG,"User tried to change their credit card to an invalid one.");
            }
            else {
                usersManagement.updateUserCreditCardNumber(currentUserEmail, newCreditCard);
                isUpdated = true;
                logger.log(Level.DEBUG, "User updated their credit card number");
            }
        }
        if (!newPassword.equals("")){
            if (newPassword.length() < Utilities.getMinPasswordLength()) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Error during the update: the password must be at least " + Utilities.getMinPasswordLength() + " characters long.);");
                out.println("</script>");
            } else {
                usersManagement.updateUserPassword(currentUserEmail, newPassword);
                isUpdated = true;
                logger.log(Level.DEBUG, "User updated their password");
            }
        }
        if (!newEmail.equals("")){
            if (!Utilities.isEmail(newEmail)) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Error during the update: the inserted email is not valid.');");
                out.println("</script>");
                logger.log(Level.DEBUG, "User tried to change their email to an invalid email");
            } else if (usersManagement.userPresent(newEmail)){
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Error during the update: the inserted email has already been used.');");
                out.println("</script>");
                logger.log(Level.DEBUG,"User tried to change their email to an already used email");
            } else {
                usersManagement.updateUserEmail(currentUserEmail, newEmail);
                logger.log(Level.DEBUG, "User updated their email");
                // Invalidate session (logout)
                request.getSession().invalidate();
                // Redirect to main page
                request.getRequestDispatcher("/index.html").include(request,response);
            }
        }

        String message = isUpdated?
                            "<div class=\"main\"> User information updated successfully </div>" :
                            "<div class=\"main\"> None of the user fields has been updated </div>";

        response.getWriter().print(message);
        rs.include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        // Check if user is authenticated
        if (session.getAttribute("username") == null)
            request.getRequestDispatcher("/index.html").forward(request, response);


        StringBuffer toPrint = new StringBuffer();
        String userInfo = usersManagement.getUserInfo(session.getAttribute("username").toString());
        List<String> fields = Arrays.asList(userInfo.split("\\|"));

        // User info
        toPrint.append("<div class=\"main\">");
        toPrint.append("<table id=\"contentTable\">");
        toPrint.append("<tr>");
        toPrint.append("<th> Name </th> <th> Surname </th> <th> Email </th> <th> Credit card </th> <th> Registration date </th>");
        toPrint.append("<tr>");
        for (String field : fields) {
            toPrint.append("<td>" + field + "</td>");
        }
        toPrint.append("</tr>");
        toPrint.append("</table>");

        // Edit info
        toPrint.append("<div style=\"margin-top: 80px;\">");
        toPrint.append("<h1> Edit information </h1>");
        toPrint.append("<br> (If you want to edit an info type into the respective field and press the confirm button)");
        toPrint.append("<form action=\"userInfo\" method=\"post\">");
        toPrint.append("<ul style=\"list-style:none;\">");
        toPrint.append("<li>Edit name:  <input type=\"text\" placeholder=\"name\" name=\"name\"/> </li>");
        toPrint.append("<li>Edit surname:  <input type=\"text\" placeholder=\"surname\" name=\"surname\"/> </li>");
        toPrint.append("<li>Edit email:  <input type=\"text\" placeholder=\"email\" name=\"email\"/> </li>");
        toPrint.append("<li>Edit credit card number:  <input type=\"text\" placeholder=\"credit card number\" name=\"creditCard\"/> </li>");
        toPrint.append("<li>Edit password: <input type=\"password\" placeholder=\"password\" name=\"password\"/> </li>");
        toPrint.append("</ul>");
        toPrint.append("<input type=\"submit\" value=\"Confirm\"/>");
        toPrint.append("</form>");
        toPrint.append("</div>");

        // Close div main
        toPrint.append("</div>");

        response.getWriter().print(toPrint.toString());
        RequestDispatcher rs = getServletContext().getRequestDispatcher("/user-personal-info.jsp");
        rs.include(request, response);
    }
}
