import org.apache.log4j.PropertyConfigurator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;


@WebServlet(name = "ServletInitLog4j")
public class ServletInitLog4j extends HttpServlet {
    public void init() {
        String prefix = getServletContext().getRealPath("/");
        String configFilePath = getInitParameter("log4j-init-file");
        if (configFilePath != null)
            PropertyConfigurator.configure(prefix + configFilePath);
    }
}
