<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Webflix</title>
</head>
<body>

<%
    // If user is not authenticated then redirect to login page
    if (session.getAttribute("username") == null)
        request.getServletContext().getRequestDispatcher("/index.html").forward(request, response);
%>

<div class="sidenav">
    <div> Webflix</div>
    <a href="suggestedContent"> Suggested content </a>
    <a href="showWatchlist">Watchlist</a>
    <a href="searchResults" class="active">Search</a>
    <a href="userInfo">Personal information</a>
    <a href="deleteUser" onclick="return confirm('Are you sure? All your traces will be erased from the system')">Delete account</a>
    <a href="logout">Logout</a>
</div>

<div class="main">
    ${results}

</body>
</html>
