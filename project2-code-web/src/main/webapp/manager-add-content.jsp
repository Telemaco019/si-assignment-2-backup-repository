<%--
  Created by IntelliJ IDEA.
  User: telemaco
  Date: 10/28/18
  Time: 9:39 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Administration main page</title>
    <link rel="stylesheet" href="css/style.css">
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            font-family: "Roboto",sans-serif;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 0px;
            margin-top: 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        .box {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
            width: 80%;
            margin:auto;
        }
    </style>
</head>
<body>

<%
    // If user is not authenticated OR
    // user is authenticated but he's not an administrator THEN
    // redirect to login page
    if (session.getAttribute("username") == null || session.getAttribute("isManager").equals(false))
        request.getServletContext().getRequestDispatcher("/index.html").forward(request, response);
%>

<div class="sidenav">
    <div> Webflix</div>
    <a href="#" class="active"> Add content </a>
    <a href="editContent">Edit content</a>
    <a href="searchResults">Search</a>
    <a href="logout">Logout</a>
</div>

<div class="main">
    <div class="box">
        <form action="addContent" method="post">
            <label for="title">Title</label>
            <input type="text" id="title" name="title" placeholder="Title of the content..."/>

            <label for="director">Director</label>
            <input type="text" id="director" name="director" placeholder="Name of the director..."/>

            <label for="year">Year</label>
            <input type="text" id="year" name="year" placeholder="Year of the content..."/>

            <label for="category">Category</label>
            <input type="text" id="category" name="category" placeholder="Category of the content..."/>

            <label for="type">Content type</label>
            <select id="type" name="type">
                <option value="movie">Movie</option>
                <option value="episode">Episode</option>
            </select>

            <input type="submit" value="Confirm"/>
        </form>
    </div>
</div>

</body>
</html>
