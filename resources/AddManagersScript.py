import mysql.connector
import hashlib


mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="root",
  database="proj2"
)

mycursor = mydb.cursor()

#################################################
# Fields of the Manager you want to add to the db
#################################################
name = "Pippo"
surname = "Baudo"
email = "pippo.baudo@gmail.com" # Must be unique, check that there isn't another manager with the same email in the db
password = "manager"
#################################################


hash_object = hashlib.sha256(password)
sha256password = hash_object.hexdigest()


sql = "INSERT INTO Manager (name, surname, password, email) VALUES (%s, %s, %s, %s)"
val = (name,surname,sha256password,email)
mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record inserted.")
