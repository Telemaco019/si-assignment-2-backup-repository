import mysql.connector
import hashlib


mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="root",
  database="proj2"
)

mycursor = mydb.cursor()


sql = "INSERT INTO Content (id, director, year, category, title, type) VALUES (%s, %s, %s, %s, %s, %s)"
val_1 = (1,"Quentin Tarantino",1997,"Action","Pulp Fiction","movie")
val_2 = (2,"Quentin Tarantino",2012,"Action","Django","movie")
val_3 = (3,"Vince Gilligan",2013,"Drama","Felina","episode")
val_4 = (4,"Alfred Hitchcock",1960,"Horror","Psycho","movie")
val_5 = (5,"Stanley Kubrick",1968,"Action","2001 a space odyssey","movie")
val_6 = (6,"Francis Ford Coppola",1972,"Drama","The Godfather","movie")
val_7 = (7,"Sergio Leone",1966,"Western","Il buono, il brutto e il cattivo","movie")
val_8 = (8,"Michael Moore",1997,"Action","Bowling a Columbine","movie")
val_9 = (9,"Charles Ferguson",2010,"Documentary","Inside Job","movie")
val_10 = (10,"Justin Roiland",2017,"Comedy","Pickle Rick","episode")
val_11= (11,"Robert Zemeckis",1986,"Comedy","Back to the future","movie")


mycursor.execute(sql, val_1)
mycursor.execute(sql, val_2)
mycursor.execute(sql, val_3)
mycursor.execute(sql, val_4)
mycursor.execute(sql, val_5)
mycursor.execute(sql, val_6)
mycursor.execute(sql, val_7)
mycursor.execute(sql, val_8)
mycursor.execute(sql, val_9)
mycursor.execute(sql, val_10)
mycursor.execute(sql, val_11)



mydb.commit()
print("records inserted.")
