import mysql.connector

rootPassword = raw_input("Insert the root password of the DB: ")
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd=rootPassword,
  database="proj2"
)

mycursor = mydb.cursor()

print("Managers present in the DB: ")
mycursor.execute("SELECT * FROM Manager")
managers = mycursor.fetchall()

if len(managers) > 0:
    for x in managers:
        print(x)
    id = raw_input("Type the ID of the manager you want to remove: ")
    query = "DELETE FROM Manager WHERE id=%s"
    val=(id,)
    mycursor.execute(query,val)
    mydb.commit()
    print(mycursor.rowcount, "record deleted.")
else:
    print("No managers in the DB")
